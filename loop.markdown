#Loops
Loops are handy, if you want to run the same code over and over again, each time with a different value.

####Instead of writing:
```javascript
text += cars[0] + "<br>";
text += cars[1] + "<br>";
text += cars[2] + "<br>";
text += cars[3] + "<br>";
text += cars[4] + "<br>";
text += cars[5] + "<br>"; 
```
####you can write:
```javascript
var i;
for (i = 0; i < cars.length; i++) {
    text += cars[i] + "<br>";
} 
```
##Different Kinds of Loops:
- **for** - loops through a block of code a number of times
- **for/in** - loops through the properties of an object
- **while** - loops through a block of code while a specified condition is true
- **do/while** - also loops through a block of code while a specified condition is true

##FOR or WHILE
FOR loops are used when we have a specific iteration number.
```javascript
for (var i = 0; i < 5; i++) {
    text += i + "<br>";
} 
//0
//1
//2
//3
//4
```
WHILE loops are used when we need to repeat an action until our action is completed.
```javascript
var spectator = 0;
while(spectator < 5) {
    text += spectator + "<br>";
    spectator++;
}
```