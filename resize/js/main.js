var myDiv = document.querySelector('div');

window.addEventListener('resize', onResize);

var idTimeout = -1;

function onResize(e) {
    clearTimeout(idTimeout);
    idTimeout = setTimeout(function() {
        myDiv.style.width = window.innerWidth * .9 + 'px';
        myDiv.style.height = window.innerHeight * .9 + 'px';
    },500);
}

onResize();