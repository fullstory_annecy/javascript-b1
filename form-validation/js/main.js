// we keep a reference to our form with myForm variable (HTMLElement)
var myForm = document.querySelector('form');
// we listen to the submit event with the onSubmit function
myForm.addEventListener('submit', onSubmit);

// we keep a reference to all inputs inside our form with the inputs variable (NodeList is an array of HTMLElement)
var inputs = document.querySelectorAll('form input:not([type="submit"])');
// we keep a reference of our error message with errorMsg variable (HTMLElement)
var errorMsg = document.querySelector('form #errorMsg');


function onSubmit(event) {
    // we prevent the default browser behavior
    event.preventDefault();

    // we set the error count to zero
    errorCount = 0;

    // we go through all inputs from the NodeList inputs
    for (var i = 0 ; i < inputs.length ; i++) {


        // we check all kind of input types (email, password, etc.)
        var isInputValid = false;

        switch (inputs[i].getAttribute('type')) {
            case 'email' :
                isInputValid = checkEmail(inputs[i].value);
                break;
            case 'password' :
                isInputValid = checkPassword(inputs[i].value);
                break;
            
        }

        // toggle will add or remove the class error from the input element
        inputs[i].classList.toggle('error',!isInputValid);

        // if input isn't valid we increment the errorCount value
        if(!isInputValid) {
            errorCount++;
        }
        
    }


    // we display a message according to errorCount value
    if(errorCount === 0) {
        errorMsg.textContent = 'Formulaire Validé !';
    }else if(errorCount === 1) {
        errorMsg.textContent = 'Votre formulaire contient 1 erreur';
    }else {
        errorMsg.textContent = 'Votre formulaire contient ' + errorCount + ' erreurs';
    }


}

// function to validate email
function checkEmail(value) {
    return value && value.indexOf('@') > 0;
}

// funtion to validate password
function checkPassword(value) {
    return value && value.indexOf('!') == -1;
}
