#Variables
JavaScript variables are containers for storing data values.

```javascript
var x = 5;
var y = 6;  
var z = x + y;  
```

x,y,z doesn't mean anything, let's use more descriptive names

```javascript
var vipSpectators = 5;
var ticketSpectators = 6;  
var totalSpectators = vipSpectators + ticketSpectators;  
```
- Names can contain letters, digits, underscores, and dollar signs.
- Names must begin with a letter
- Names can also begin with $ and _
- Names are case sensitive (y and Y are different variables)
- Reserved words (like JavaScript keywords) cannot be used as names

###declare a variable
```javascript
var spectator; 
```
the default value after assignment is "undefined"

###assign a variable
```javascript
spectator = 5;
```

###assign & declar
```javascript
var spectator = 5;
```

### Other way to create variable
####mutli variables
```javascript
var spectator = 5, rooms = 3, price = 20;
```
####multiline
```javascript
var spectator = 5,
    rooms = 3,
    price = 20;
```

##Arithmetic
####operators
#####addition
```javascript
var x = 2 + 4 + 5;
```
#####subtraction
```javascript
var x = 2 - 4 - 5;
```

#####Multiplication
```javascript
var x = 2 * 4 * 5;
```

#####Division
```javascript
var x = 2 / 4 / 5;
```

#####Modulus
```javascript
var x = 2 % 5;
```

#####Increment
```javascript
x++
```

#####Decrement
```javascript
x--
```
####Assignment Operators
Operator | Example | Same As
--- | --- | ---
= | x = y | x = x
+= | x += y | x = x + y
-= | x -= y | x = x -y
*= | x *= y | x = x * y
/= | x /= y | x = x / y
%= | x %= y | x = x % y

####Comparison Operators
Operator | Description
--- | --- 
== | equal to
=== | equal value and equal type
!= | not equal
!== | not equal value or not equal type
> | greater than
< | less than
>= | greater than or equal to
<= | less than or equal to
? | ternary operator

####Logical Operators
Operator | Description
--- | --- 
&& | logical and
&#124;&#124; | logical or
! | logical not

##Data Types
JavaScript variables can hold many data types: numbers, strings, objects and more:

Variable | Type
--- | ---
var spectators = 16; | Number
var bandName = "Tame Impala"; | String
var concert = {place : "Paris", tickets : 2349}; | Object

#####String
```javascript
var festivalName = "Coachella";
```
or
```javascript
var festivalName = 'Coachella';
```

#####Numbers
```javascript
var festivalDay = 19;
```

#####Boolean
```javascript
var areTicketsAvalaible = true;
var areTicketsAvalaible = false;
```
or
```javascript
var areTicketsAvalaible = 1;
var areTicketsAvalaible = 0;
```

#####Array
```javascript
var bands = ["U2", "Gorillaz", "PNL"];
```

#####Object
```javascript
var singer = {firstName:"Freddie", lastName:"Mercury", age:50, eyeColor:"blue"};
```