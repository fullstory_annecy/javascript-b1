#Conditions
Conditional statements are used to perform different actions based on different conditions.
- Use **if** to specify a block of code to be executed, if a specified condition is true
- Use **else** to specify a block of code to be executed, if the same condition is false
- Use **else if** to specify a new condition to test, if the first condition is false
- Use **switch** to specify many alternative blocks of code to be executed

###if syntax
```javascript
if (condition) {
    block of code to be executed if the condition is true
}  
```

###else syntax
```javascript
if (condition) {
    block of code to be executed if the condition is true
} else {
    block of code to be executed if the condition is false
}
```

###else if syntax
```javascript
if (condition1) {
    block of code to be executed if condition1 is true
} else if (condition2) {
    block of code to be executed if the condition1 is false and condition2 is true
} else {
    block of code to be executed if the condition1 is false and condition2 is false
}
```

###switch syntax
```javascript
switch(expression) {
    case x:
        code block
        break;
    case y:
        code block
        break;
    default:
        code block
} 
```
