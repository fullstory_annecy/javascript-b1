
var maxCase = 20;

for (var i = 0 ; i < maxCase ; i++) {

    // creer les éléments HTML
    var div = document.createElement('div');
    div.setAttribute('id',"case" + i);
    div.setAttribute('class',"case");
    document.body.append(div);

    // lier les éléments à des classes
    var myCase = new Case(i,"#case" + i);
    myCase.ui.addEventListener('move', onMove);
}


var myPion = new Pion('#pion');

function onMove(e) {
    console.log('onMove',e.currentTarget.offsetLeft);
    myPion.moveTo(e.currentTarget.offsetLeft,e.currentTarget.offsetTop);
}

var position = 0;
var myDice = new Dice();

var myButton = document.querySelector('#myButton');
myButton.addEventListener('click',onDiceClick)

function onDiceClick() {

    position += myDice.getRandom();

    var nextCase;

    if(position >= maxCase) {
        nextCase = document.querySelector('#case' + (maxCase - 1));
        alert("win");
    }else {
        nextCase = document.querySelector('#case' + position);
    }
    myPion.moveTo(nextCase.offsetLeft,nextCase.offsetTop);

    console.log('myDice position','#case' + position );

}