class Case {
    constructor(id,selector) {
        this.index = id;
        this.ui = document.querySelector(selector);
        console.log('Case',this.ui);
        this.ui.addEventListener('click',this.onClick.bind(this));
    }

    onClick() {
        console.log('click',this);
        this.ui.dispatchEvent(new Event('move'))
    }
}