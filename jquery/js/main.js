var hiddenBox = $( "#banner-message" );
hiddenBox.hide();

$( "#button-container #toggle" ).on( "click", function( event ) {
    hiddenBox.toggle();
});

$( "#button-container #hide" ).on( "click", function( event ) {
    hiddenBox.hide();
});

$( "#button-container #show" ).on( "click", function( event ) {
    hiddenBox.show();
});

$( "#button-container #slide" ).click(function() {
    hiddenBox.slideDown( "slow", function() {
        console.log('animation complete !');
    });
});
var allDiv = $("#accordeon div");
allDiv.hide();


$("#button1 + div").delay(1000).slideDown().delay(5000).slideUp();

/*

// ACCORDEON
var allButtons = $("#accordeon button");
var allDiv = $("#accordeon div");

allDiv.hide();

allButtons.on('click', function (event) {

    for (var i = 0 ; i < allButtons.length; i++) {
        if(allButtons[i] !== event.currentTarget) {
            $("+ div",allButtons[i]).slideUp();
        }
    }

    $("+ div",event.currentTarget).slideToggle();
});
*/

