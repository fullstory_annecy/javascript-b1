##Functions
A JavaScript function is a block of code designed to perform a particular task.
```javascript
function myFunction(p1, p2) {
    return p1 * p2;              // The function returns the product of p1 and p2
}
```